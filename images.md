# Sección inicio:

- 3 imágenes,  1 por banner. Van ubicadas como background-image en el `li.rdnt-banner-home-li` (insertadas en el pug/ya las borré del css)

# Promociones:
- 3 imágenes, 1 por banner pero se repite en 2 divs . El padre `.li.rdnt-banner-promo-slider-li` tiene 2 div llamados `.rdnt-banner-photo` y `.rdnt-banner-promo` los que reciben una imagen como background-image y se repite en ambas la misma, pero el primer li tiene la primera imagen, el segundo la segunda y el tercero la tercera (ya los dejé insertados como estilo inine en el pug y los comenté en el scss)

# Especialidades:
- Cambia una foto de la reseña corta de la especialidad que está ubicada como imagen en el html en el div `.rdnt-especialities-picture` y cambia un background-image que está ubicado en el modal de la especialidad en el div llamado: `.rdnt-banner-modal-especialities` (ya lo integré como background image en el pug y lo comenté en el sass)

# Sedes: 

- Son 4 imágenes por sede (3 sedes) que se repiten dentro de los siguientes divs y van de la siguiente manera:


## Banner Sede (el div padre en el banner 1 es `li.banner-venues-li.venues-banner-1` en el banner 2 es `li.banner-venues-li.venues-banner-2` y en el banner 3 es `li.banner-venues-li.venues-banner-3` )

Imagen 1 | Imagen 2 | Imagen 3 | Imagen 4
------------ | ------------- | ------------- | -------------
`.rdnt-venues-box-img.thumb-banner-img-1` => imagen html normal <br/> `.rdnt-venue-banner-img.banner-img-1` => background image inline <br/> `.rdnt-venue-img.venue-1` => background image inline (en el modal **venue selection modal** y modal **venue taken selection modal** | `.rdnt-venues-box-img.thumb-banner-img-2` => imagen html normal <br/> `.rdnt-venue-banner-img.banner-img-2` => background image inline | `.rdnt-venues-box-img.thumb-banner-img-3` => imagen html normal <br/> `.rdnt-venue-banner-img.banner-img-3` => background image inline | `.rdnt-venues-box-img.thumb-banner-img-4` => imagen html normal <br/> `.rdnt-venue-banner-img.banner-img-4` => background image inline |


*Los nombres de estos div son importantes porque el js va al padre, rescata el número y luego de eso se dirije a los hijos y cuando haces click en una foto vuelve a revisar esta herencia para saber que foto marcar como activa así que no hay que tocar los nombres de estos div*

*Sólo la primera imagen de cada sede es la que aparece en los modales de reserva tu hora y consulta hora reservada, el consulta hora reservada no tienes de donde previsualizarlo proque el cliente me dijo que lo deje desactivado por ahora pero es igual al otro solo que cambia el título*

# Nosotros:
- Ya lo tienes resuelto pero tal cual lo tienes es una imagen dentro del html normal en el div `.rdnt-us-picture` de cada li 

# Empresas 

- Sólo se cambia la imagen de fondo `rdnt-enterprise-banner-li` que es de tipo background image

