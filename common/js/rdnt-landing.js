$(document).ready(function(){

  //SCROLL BEHAVIOR FOR MODALS 
  function lockScroll(){
    $html = $('html'); 
    $body = $('body'); 
    var initWidth = $body.outerWidth();
    var initHeight = $body.outerHeight();

    var scrollPosition = [
        self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
        self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
    ];
    $html.data('scroll-position', scrollPosition);
    $html.data('previous-overflow', $html.css('overflow'));
    $html.css('overflow', 'hidden');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);   

    var marginR = $body.outerWidth()-initWidth;
    var marginB = $body.outerHeight()-initHeight; 
    $body.css({'margin-right': marginR,'margin-bottom': marginB});
  } 

  function unlockScroll(){
    $html = $('html');
    $body = $('body');
    $html.css('overflow', $html.data('previous-overflow'));
    var scrollPosition = $html.data('scroll-position');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);    

    $body.css({'margin-right': 0, 'margin-bottom': 0});
  }

  $('.rdnt-venues .unslider-nav ol li[data-slide="0"]').click(function(){
    // console.log("entre 1");
    sliderVenue.unslider('animate:0');
  });
  $('.rdnt-venues .unslider-nav ol li[data-slide="1"]').click(function(){
    sliderVenue.unslider('animate:1');
    // console.log("entre 2");
  });
  $('.rdnt-venues .unslider-nav ol li[data-slide="2"]').click(function(){
    sliderVenue.unslider('animate:2');
    // console.log("entre 3");
  });

  // CLOSE CURRENT MODAL 
  $("span.rdnt-close-modal").click(function() {
    $('.rdnt-modal-bg').fadeOut("fast");
    var thisModal = $(this).parent().parent();
    $(thisModal).removeClass("display-flex");
    $(thisModal).addClass("display-none");
    unlockScroll();
  });

  // CHANGE ACTIVE IMAGE BANNER
  $(".rdnt-venues-box-img").click( function(){
    var currentClickImg = $(this)[0].classList[1].match(/\d+$/);
    var currentBanner = $(this).closest("li")[0].classList[1].match(/\d+$/);
    var selectedBanner = ".venues-banner-"+currentBanner
    var selectedImg = ".banner-img-"+currentClickImg
    var changeActiveBanner = selectedBanner + " " + selectedImg
    var removeActiveBanner = selectedBanner + " .rdnt-venue-banner-img"
    var removeActiveThumb = selectedBanner + " .rdnt-venues-box-img"
    $(removeActiveThumb).removeClass("active");
    $(this).addClass("active");
    $(removeActiveBanner).removeClass("active");
    $(changeActiveBanner).addClass("active");
  });

  //TRIGGER MOBILE MENU
   $(".rdnt-mobile-trigger").click(function(){
    if($(".rdnt-menu").hasClass("display"))
        {
          $(".rdnt-menu").removeClass("display");
          $(".rdnt-reservation").addClass("display-none");
          
        }
    else{
          $(".rdnt-menu").addClass("display");
          $(".rdnt-reservation").removeClass("display-none");
          $(".rdnt-reservation").addClass("display-flex");
        }
  });
  //ACTIVATE LINKS WHEN IS CLICKED
  $("li.rdnt-menu-li").click(function(){
    $("li.rdnt-menu-li").removeClass("active");
    $(".rdnt-menu").addClass("display");
    $(this).addClass("active");
    $(".rdnt-reservation").removeClass("display-none");
    $(".rdnt-reservation").addClass("display-flex");
  });
});

$(document).ready(function(){
  function moveVenues() {
    if ( window.outerWidth > 480 ) {
      var activeVenue = $('.rdnt-venues-new-section-venue.active');
      var lastVenueCount = $('.rdnt-venues-new-section-venue:last-of-type').index();

      if ( activeVenue.index() != lastVenueCount ) {
        $('.rdnt-venues-new-section-venue').removeClass('active');
        $(activeVenue).next().addClass('active');

        //CHANGE TAB
        $('.rdnt-new-nav-venues-li').removeClass('active');
        var activateTab = activeVenue.index() + 2;
        var tab = $('.rdnt-new-nav-venues-li:nth-of-type(' + activateTab + ')');
        
        $(tab).addClass('active');

      }
      else {
        $('.rdnt-venues-new-section-venue').removeClass('active');
        $('.rdnt-venues-new-section-venue:first-of-type').addClass('active');
        //CHANGE TAB
        $('.rdnt-new-nav-venues-li').removeClass('active');
        $('.rdnt-new-nav-venues-li:first-of-type').addClass('active');
      }
    }
  }
  var createInterval = function() { return setInterval(moveVenues, 4500) };
  var moveVenuesTimer = createInterval();
  moveVenuesTimer;

  //ACTIVE VENUES 
  $('.rdnt-new-nav-venues-li').click(function(){
    clearInterval(moveVenuesTimer);
    $('.rdnt-new-nav-venues-li').removeClass('active');
    $(this).addClass('active');
    var activeTab = $(this).index() + 1;
    var activateVenue = $('.rdnt-venues-new-section-venue:nth-of-type(' + activeTab + ')')
    $(".rdnt-venues-new-section-venue").removeClass('active');
    $(activateVenue).addClass('active');
    moveVenuesTimer = createInterval();
  })
  

  $('.rdnt-partner-brand-box').click(function(){
    //MOVE ACTIVE INDICATOR
    $('.rdnt-partner-brand-box').removeClass('active');
    $(this).addClass('active');
    //SET PARAGRAPH TO ACTIVE
    var brandClicked = $(this).index();
    var textShow = $('.rdnt-brand-text')[brandClicked];
    $('.rdnt-brand-text').removeClass('active');
    $(textShow).addClass('active');
  });

  var mobileCheck = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || window.outerWidth < 480 ;

  //IF ITEMS ARE 4 OR LESS WIDTH IS DEFAULT
  if ($('.rdnt-partner-brand-box:last-of-type').index() <=3) {
    //DETECT MOBILE OR RESPONSIVE 
    if(mobileCheck) {
      $('.rdnt-rotator-box-img').width(480);
    }
    else {
      $('.rdnt-rotator-box-img').width(480);
    }
  }
  //ELSE INCREASE ROTATOR WIDTH IN 120PX BY EACH ITEM FOUND
  else {
    var widthRotator = $('.rdnt-rotator-box-img').width();
    var multiplyWidth = $('.rdnt-partner-brand-box:last-of-type').index() + 1;
    if (mobileCheck) {
      var finalWidth = multiplyWidth * 80;
    }
    else {
      var finalWidth = multiplyWidth * 120;
    }
    

    $('.rdnt-rotator-box-img').width(finalWidth);
  }

  function moveBrands() {
    //GET THE MAGIC NUMBERS
    var mobileCheck = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || window.outerWidth < 480 ;
    if (mobileCheck) {
      var rotatorWidth = $('.rdnt-rotator-box-img').width() - 320;
    }
    else {
      var rotatorWidth = $('.rdnt-rotator-box-img').width() - 480;
    }
    var marginLeftPx = $('.rdnt-rotator-box-img').css('margin-left');
    var marginLeftMinus = parseInt(marginLeftPx);
    var marginLeft = Math.abs(marginLeftMinus);
    //CONDITIONAL CYCLE
    if (marginLeft < rotatorWidth) {
      if (mobileCheck) {
        $('.rdnt-rotator-box-img').animate({marginLeft: '-=320px'}, 500);
      }
      else {
        $('.rdnt-rotator-box-img').animate({marginLeft: '-=480px'}, 500);
      }
    }
    else {
      $('.rdnt-rotator-box-img').animate({marginLeft: '0'}, 500);
    }
  }
  setInterval(moveBrands, 5000);
});

function closeNewVenueSelectionModal(){
  $(".rdnt-venue-new-modal-bg").fadeOut("slow");
  $(".venue-selection-new-modal").addClass("display-none");
  unlockScroll();
}

function closeNewVenueSelectionReserveModal(){
  $(".rdnt-venue-new-modal-bg").fadeOut("slow");
  $(".reserve-modal").addClass("display-none");
  unlockScroll();
}

function openNewVenueSelectionReserveModal(){
  $(".rdnt-venue-new-modal-bg").fadeIn("fast");
  $(".reserve-modal").removeClass("display-none");
  lockScroll();
}
