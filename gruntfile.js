module.exports = function(grunt) {
    grunt.initConfig({
      sass: {
        dist: {
          files: [{
            expand: true,
            cwd: 'src/styles/',
            src: ['*.scss'],
            dest: 'dist/styles',
            ext: '.css'
          }]
        }
      },
      postcss: {
        options: {
          map: false,
          processors: [
            require('autoprefixer')({browsers: 'last 2 versions'}),
            require('cssnano')()
          ]
        },
        dist: {
          src: 'dist/styles/*.css'
        }
      },
      watch: {
        options: {
          livereload: 9090,
        },
        sass: {
          files: ['src/styles/*.scss'],
          tasks: ['sass'],
        }
      }
    });
    
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default',['sass', 'postcss' ,'watch']);
  };